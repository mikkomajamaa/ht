Kansion sis�lt�:
Mikko_Majamaa_olio-ht-raportti: Olio-osuuden raportti
Mikko_Majamaa_olio-ht-demo: Olio-osuuden videodemo
Mikko_Majamaa_tk-ht-raportti: Tietokantaosuuden raportti
Mikko_Majamaa_tk-ht-demo: Tietokantaosuuden videodemo
ht: SQLite 3 -tietokanta, johon on sy�tetty SmartPosteihin liittyv� parsittu XML-data, nelj� esinett� sek� l�hetysluokat. Tietokanta, joka on siis valmis k�ytett�v�ksi graafisen
k�ytt�liittym�n/SQLite 3:n kautta.
dump.txt: Tiedosto luotu .dump-komennolla em. ht-tietokannasta.
schema.txt: Tiedosto luotu .schema-komennolla em. ht-tietokannasta.
SQL-statements.txt: Kaikki k�ytetyt SQL-SELECTit, -UPDATEt, -INSERTit, -DELETEt.